#ifndef HEADER_H_
#define HEADER_H_

#include <iostream>
using std::ostream;

namespace VECTOR
{
    class Vector{
    private:
        double x;
        double y;
        double len;
        double ang;
        char mode;
        void set_x();
        void set_y();
        void set_len();
        void set_ang();
    public:
        Vector();
        Vector(double n1,double n2, char form='r');
        void set(double n1, double n2, char form='r');
        double xval() const{return x;}
        double yval() const {return y;}
        double lenval() const {return len;}
        double angval() const {return ang;}
        void polar_mode();
        void rect_mode();
        Vector operator+(const Vector & v) const;
        Vector operator-(const Vector & v) const;
        Vector operator-() const;
        Vector operator*(double n) const;
        friend Vector operator*(double n, const Vector & v);
        friend ostream & operator<<(ostream & os,const Vector & v);
        ~Vector();
    };
}



#endif
