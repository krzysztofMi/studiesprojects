#include <cmath>
#include "header.h"

namespace VECTOR
{
    const double Rad_to_deg = 57.2957795130823;

    void Vector::set_len()
    {
        len = sqrt(x*x + y*y);
    }
    void Vector::set_ang()
    {
        if(x==0.0 && y==0.0)
            ang = 0.0;
        else
            ang = atan2(y,x);
    }
    void Vector::set_x()
    {
        x = len * cos(ang);
    }
    void Vector::set_y()
    {
        y = len * sin(ang);
    }
    Vector::Vector()
    {
        x=y=len=ang=0.0;
        mode ='r';
    }
    Vector::Vector(double n1,double n2, char form)
    {
        mode = form;
        if(form == 'r')
        {
            x=n1;
            y=n2;
            set_len();
            set_ang();
        }
        else if(form == 'p')
        {
            len = n1;
            ang = n2;
            set_x();
            set_y();
        }
        else
        {
            std::cerr << "Niepoprawne trzeci argument Vector() - zeruje dane wejsciowe.\n";
            x=y=len=ang=0.0;
            mode ='r';
        }
    }
    void Vector::set(double n1, double n2, char form)
    {
         mode = form;
        if(form == 'r')
        {
            x=n1;
            y=n2;
            set_len();
            set_ang();
        }
        else if(form == 'p')
        {
            len = n1;
            ang = n2;
            set_x();
            set_y();
        }
        else
        {
            std::cerr << "Niepoprawne trzeci argument Vector() - prosze sprobowac ponownie.\n";
        }
    }
    void Vector::polar_mode()
    {
        mode = 'p';
    }
    void Vector::rect_mode()
    {
        mode = 'r';
    }
    Vector Vector::operator+(const Vector & v) const
    {
        return Vector (x+v.x,y+v.y);
    }
    Vector Vector::operator-(const Vector & v) const
    {
        return Vector (x-v.x,y-v.y);
    }
    Vector Vector::operator-() const
    {
        return Vector (-x,-y);
    }
    Vector Vector::operator*(double n) const
    {
        return Vector (n*x,n*y);
    }
    Vector operator*(double n, const Vector & v)
    {
        return v*n;
    }
    ostream & operator<<(ostream & os,const Vector & v)
    {
        if(v.mode =='p')
        {
            os << "Dlugosc : "<< v.len
            << " Kat: "<<v.ang*Rad_to_deg;
        }
        else if(v.mode =='r')
        {
            os<<"(x,y) = ("
            <<v.x<<","<<v.y<<")";
        }
        else
            os<<"Niepoprawny typ reprezentacj wektora";
        return os;
    }
    Vector::~Vector()
    {

    }
}
