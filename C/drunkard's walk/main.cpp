#include "header.h"
#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;
//Simple drunkard's walk simulator
int main()
{
using VECTOR::Vector;

srand(time(0));
double direction;
Vector step;
Vector result(0.0,0.0);
unsigned long steps = 0;
double target;
double dstep;
cout << "Podaj dystans do przejscia (k, aby zakonczyc) ";
while(cin >> target)
{
    cout<<"Podaj dlugosc korku: ";
    if (!(cin >> dstep))
        break;
    while(result.lenval() < target)
    {
        direction = rand() % 360;
        step.set(dstep,direction,'p');
        result =result + step;
        steps ++;
    }

    cout << "Po "<<steps <<" krokach obiekt osiagnal polozenie: ";
    cout << result<<endl;
    result.polar_mode();
    cout << " czyli\n"<<result<<endl;
    cout << "Srednia dlugosc kroku delikwenta: ";
    cout << result.lenval()/steps <<endl;
    steps = 0;
    result.set(0.0,0.0);
    cout << "Podaj dystans do przejscia (k, aby zakonczyc) ";
}
cout << "Koniec! ";
return 0;
}
