#include <stdio.h>

#include <string.h>
#include "operacje.h"
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/sem.h>

int main(int argc, char *argv[])
{
    key_t klucz1, klucz2;
    int X=5;
    int M=5;
    int semID;
    int N=6;
    int i;
    int numer;
    int shmID;
    char bufor[3];
    int *widelec;
    if((klucz1=ftok(".",'A'))==-1)
    {
        printf("Ftok1 nie dzia�a");
        exit(1);
    }
    semID=alokujSemafor(klucz1,N,IPC_CREAT | 0666);
    if((klucz2=ftok(".",'B'))==-1)
    {
        printf("Ftok2 nie dzia�a");
        exit(1);
    }
    shmID=shmget(klucz1,5*sizeof(int),IPC_CREAT|0666);
    if(shmID==-1)
    {
        printf("blad shm");
        exit(1);
    }
    fflush(stdout);
    widelec = (int*)shmat(shmID,0,0);
    numer=atoi(argv[1]);
    for(i=0;i<M;i++)
    {
        printf("%d. rozmy�la\n",numer);
        waitSemafor(semID,5,SEM_UNDO);
        printf("%d. wchodzi %d raz\n",numer,i);
        waitSemafor(semID,numer,SEM_UNDO);
        waitSemafor(semID,(numer+1)%5,SEM_UNDO);
        widelec[numer]=numer;
        printf("%d. spo�ywa\n",numer);
        widelec[(numer+1)%5]=-1;
        widelec[numer]=-1;
        signalSemafor(semID,numer);
        signalSemafor(semID,(numer+1)%5);
        signalSemafor(semID,5);
        printf("%d. opuszcza jadalnie\n",numer);
    }
return 0;
}










