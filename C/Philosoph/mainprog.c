#include <stdio.h>
#include <string.h>
#include "operacje.h"
#include <sys/shm.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/wait.h>
#include <unistd.h>

int main()
{
key_t klucz1, klucz2;
int semID;
int N = 6;
int i;
int shmID;
char bufor[3];
int *widelec;
if((klucz1=ftok(".",'A'))==-1)
{
    printf("Blad ftok1 mainowy");
}
semID=alokujSemafor(klucz1,N,IPC_CREAT|IPC_EXCL|0666);
for(i=0;i<5;i++)
{
    inicjalizujSemafor(semID,i,1);
}
    inicjalizujSemafor(semID,5,4);
    klucz2=ftok(".",'B');
    shmID=shmget(klucz2,5*sizeof(int),IPC_CREAT|IPC_EXCL|0666);
    if(shmID==-1)
    {
        printf("bład shm mainowy");
        exit(1);
    }
    fflush(stdout);
    widelec=(int*)shmat(shmID,NULL,0);
    for(i=0;i<5;i++) widelec[i]=-1;
    for(i=0;i<5;i++)
    {
        switch(fork())
        {
        case -1:
            perror("Blad fork mainowy");
            exit(2);
        case 0:
            sprintf(bufor,"%d",i);
            execl("./filozof","filozof",bufor,NULL);
        }
    }
    for(i=0;i<5;i++)
        wait(NULL);
    zwolnijSemafor(semID,N);
    shmctl(shmID,IPC_RMID,NULL);
    printf("Main: KONIEC\n");
return 0;
}










