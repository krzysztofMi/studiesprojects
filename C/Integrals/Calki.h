#ifndef CALKI_H_
#define CALKI_H_


extern const int LP; /*1000*/
extern const int PUNKTY; /*10000*/
extern double x_od,x_do;


double mProstokotow(double (*f)(double));
double mTrapezow(double (*f)(double));
double MC(double (*f)(double));
double randomPoint(double, double);


#endif /*CALKI_H_*/
