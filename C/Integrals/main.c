#include <stdio.h>
#include "Calki.h"
#include <stdlib.h>
#include <time.h>
#include <math.h>

double f1(double x)
{
    return (-x*x);
}

double f2(double x)
{
    return (sin(x)*cos(x));
}

double f3(double x)
{
    return (log(x)+5*x);
}

int main()
{
    srand(time(0));

    printf("\t\t\tProgram do calkowania 3 metodami\n\n");
    printf("Podaj przedzial calkowania:\n");
    printf("od=");
    scanf("%lf",&x_od);
    printf("do=");
    scanf("%lf",&x_do);

    while(x_do<=x_od)
    {
        printf("Wartosc poczatkowa musi byc mniejsza od koncowej\n\n");
        printf("Podaj przedzial calkowania:\n");
        printf("od=");
        scanf("%lf",&x_od);
        printf("do=");
        scanf("%lf",&x_do);
    }
    printf("Funkcja 1:\n\n");
    printf("Metoda prostokotow: %f\n",mProstokotow(f1));
    printf("Metoda trapezow: %f\n",mTrapezow(f1));
    printf("Metoda Monte Carlo: %f\n\n",MC(f1));

    printf("Funkcja 2:\n\n");
    printf("Metoda prostokotow: %f\n",mProstokotow(f2));
    printf("Metoda trapezow: %f\n",mTrapezow(f2));
    printf("Metoda Monte Carlo: %f\n\n",MC(f2));

    printf("Funkcja 3:\n\n");
    printf("Metoda prostokotow: %f\n",mProstokotow(f3));
    printf("Metoda trapezow: %f\n",mTrapezow(f3));
    printf("Metoda Monte Carlo: %f\n",MC(f3));


return 0;
}
