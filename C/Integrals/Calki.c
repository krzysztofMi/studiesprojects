#include <stdio.h>
#include "Calki.h"
#include <stdlib.h>


double x_od,x_do;
const int LP = 1000;
const int PUNKTY = 10000;

double mProstokotow(double (*f)(double))
{
    double dx, i, calka = 0;
    dx=(x_do-x_od)/LP;
    for(i = x_od; i <= x_do ;i+=dx)
    {
        calka+=f(i);
    }
    calka*=dx;
    return calka;
}

double mTrapezow(double (*f)(double))
{
    double dx, i, calka = 0;
    dx=(x_do-x_od)/LP;
    for(i = x_od; i < x_do ;i+=dx)
    {
        calka+=f(i)+f(i+dx);
    }
    return calka*dx/2;
}

double MC(double (*f)(double))
{
    double ymin, ymax, y, x, i, krok, calka, trafione = 0;
    ymin=ymax =f(x_od);
    krok = (x_do-x_od)/LP;
    for(x = x_od; x<=x_do; x+=krok)
    {
        y = f(x);
        if(y<ymin) ymin = y;
        else if (y>ymax) ymax = y;
    }

    if(ymin*ymax > 0)
    {
       if(ymax>0)
         ymin = 0;
       else
         ymax = 0;
    }
    for(i = 0; i<PUNKTY; i++)
    {
        x = randomPoint(x_od,x_do);
        y = randomPoint(ymin,ymax);
        if(y>0&&y<f(x))
            trafione ++;
        else if(y<0 && y>f(x))
            trafione --;
    }
    calka = (x_do-x_od)*(ymax-ymin)*trafione/PUNKTY;
    return calka;
}

double randomPoint(double x, double y)
{
return x + (double)rand()/(double)(RAND_MAX) * (y-x);
}
