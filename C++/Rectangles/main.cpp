#include <iostream>
#include "geometry.h"

main(){

    //INICJALIZACJA PROSTOKATOW
    //PROSTOKAT 1
    std::cout<<"Prosze podac wsporzedne 1 prostokata, jego szerokosc i wysokosc:\n";
    Punkt punkt1;
    double szerokosc1;
    double wysokosc1;
    std::cin>> punkt1 >> szerokosc1 >> wysokosc1;
    Prostokat prostokat1{punkt1, szerokosc1, wysokosc1};

    //PROSTOKAT 2
    std::cout<<"Prosze podac wspolrzedne 2 prostokata, jego szerokosc i wysokosc:\n";
    Punkt punkt2;
    double szerokosc2;
    double wysokosc2;
    std::cin>> punkt2 >> szerokosc2 >> wysokosc2;
    Prostokat prostokat2{punkt2, szerokosc2, wysokosc2};

    //KOMENTARZ
    //prostokat1.wyswietl_w_linii();
    //prostokat2.wyswietl_w_linii();
    if(prostokat1.czy_zawiera(prostokat2)){
        Punkt dolnyWypadkowy = prostokat1.dajPunkt().oblicz_punkt_wypadkowy(prostokat2.dajPunkt(), 'D');
        Punkt gornyWypadkowy = prostokat1.daj_prawy_gorny().oblicz_punkt_wypadkowy(prostokat2.daj_prawy_gorny(), 'G');
        double wysokosc;
        double szerokosc;
        szerokosc = dolnyWypadkowy.odleglosc_punktow(gornyWypadkowy, wysokosc);

        Prostokat wypadkowy {dolnyWypadkowy, szerokosc, wysokosc};
        wypadkowy.wyswietl_w_linii();
    }
    else{
        Punkt p;
        Prostokat wypadkowy{p, 0, 0};
        wypadkowy.wyswietl_w_linii();
    }
    return 0;
}
