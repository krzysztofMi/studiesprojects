

//PUNKT
class Punkt{
    double m_x;
    double m_y;

public:
    Punkt();
    Punkt(double, double);

    double dajX();
    double dajY();
    void wyswietl_punkt();
    Punkt oblicz_punkt_wypadkowy (Punkt, const char polozenie);
    double odleglosc_punktow (Punkt, double&);//odleglosci jedno wymiarowe x i y.

    friend std::istream& operator>> (std::istream &, Punkt &);
    friend std::ostream& operator<< (std::ostream &, const Punkt &);
};

//PROSTOKAT
class Prostokat{
    Punkt m_punkt;
    double m_szerokosc;
    double m_wysokosc;

public:
    Prostokat();
    Prostokat(Punkt &, double, double);

    Punkt dajPunkt();
    void wyswietl_prostokat();
    void wyswietl_w_linii();
    bool czy_zawiera(Prostokat &);


    Punkt daj_prawy_gorny();
};
