#include <iostream>
#include <cmath>
#include "geometry.h"

//PUNKT METODY
Punkt::Punkt():m_x{0.0}, m_y{0.0}{}
Punkt::Punkt(double x, double y): m_x{x}, m_y{y}{}

double Punkt::dajX(){return m_x;}
double Punkt::dajY(){return m_y;}

void Punkt::wyswietl_punkt(){
    std::cout<<"["<<m_x<<", "<<m_y<<"]\n";
}

//polozenie: D - lewy dolny rog G - prawy gorny rog
Punkt Punkt::oblicz_punkt_wypadkowy(Punkt p,const char polozenie){
    switch(polozenie){
        case 'D':
        {
            double x = m_x > p.dajX() ? m_x : p.dajX();
            double y = m_y > p.dajX() ? m_y : p.dajY();
            Punkt a{x,y};
            return a;
        }
        case 'G':
        {
            double x = m_x < p.dajX() ? m_x : p.dajX();
            double y = m_y < p.dajX() ? m_y : p.dajY();
            Punkt a{x,y};
            return a;
        }
        default:
            std::cout<<"Podana zly 2 parametr funkcji prosze podac G lub D lub zostaje zwocony punkt domyslny (0,0).\n";
    }
    Punkt a;
    return a;
}

double Punkt::odleglosc_punktow(Punkt punkt, double &wysokosc){
    wysokosc = abs(punkt.m_y - m_y);
    double szerokosc = abs(punkt.m_x - m_x);
    return szerokosc;
}

//funkcje zaprzyjaznione dla Punktu
std::istream& operator>>(std::istream &in, Punkt &punkt){
    in >> punkt.m_x;
    in >> punkt.m_y;

    return in;
}

std::ostream& operator<< (std::ostream &out, const Punkt &punkt){
    out << punkt.m_x << " " << punkt.m_y;
    return out;
}

//PROSTOKAT METODY
Prostokat::Prostokat(): m_punkt{0.0, 0.0}, m_szerokosc{0.0}, m_wysokosc{0.0}{}
Prostokat::Prostokat(Punkt &p, double szerokosc, double wysokosc):m_punkt{p}, m_szerokosc{szerokosc}, m_wysokosc{wysokosc}{}

Punkt Prostokat::dajPunkt(){
    double x {m_punkt.dajX()};
    double y {m_punkt.dajY()};
    Punkt punkt{x,y};
    return punkt;
}

void Prostokat::wyswietl_prostokat(){
    std::cout<<"Prostokat o wysokosci: "<< m_wysokosc <<", szerokosci: "<<m_szerokosc<<
    "\nJego lewy dolny rog znajduje sie w punkcie: "<<m_punkt;
}

void Prostokat::wyswietl_w_linii(){
    std::cout<<m_punkt<<" "<<m_szerokosc<<" "<<m_wysokosc<<std::endl;
}

bool Prostokat::czy_zawiera(Prostokat &prostokat){

    if(m_punkt.dajX()<prostokat.m_punkt.dajX()){
        if((m_punkt.dajX() + m_szerokosc)>prostokat.m_punkt.dajX()){
            if(m_punkt.dajY()<prostokat.m_punkt.dajY()){
                if((m_punkt.dajY() + m_wysokosc)>prostokat.m_punkt.dajY()){
                    return true;
                }
            }
            else{
                if((prostokat.m_punkt.dajY() + prostokat.m_wysokosc)>m_punkt.dajY()){
                    return true;
                }
            }
        }
     }
     else{
        if((prostokat.m_punkt.dajX() + prostokat.m_szerokosc)>m_punkt.dajX()){
            if(m_punkt.dajY()<prostokat.m_punkt.dajY()){
                if((m_punkt.dajY() + m_wysokosc)>prostokat.m_punkt.dajY()){
                    return true;
                }
            }
            else{
                if((prostokat.m_punkt.dajY() + prostokat.m_wysokosc)>m_punkt.dajY()){
                    return true;
                }
            }
        }
     }
    return false;
}



Punkt Prostokat::daj_prawy_gorny(){
    double x = m_punkt.dajX() + m_szerokosc;
    double y = m_punkt.dajY() + m_wysokosc;
    Punkt punkt{x,y};
    return punkt;
}
