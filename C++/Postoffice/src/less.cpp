#include "less.h"

bool Less::operator()(const std::shared_ptr<IClient> &client1,
               const std::shared_ptr<IClient> &client2) const{
    return client2->getPriority() < client1->getPriority();
}
