#include <iostream>
#include "postoffice.h"
#include "client.h"
#include "exceptions.h"
#include <fstream>

std::shared_ptr<IPostOffice> IPostOffice::create(unsigned gate_count){
    PostOffice post{gate_count};
    std::shared_ptr<IPostOffice> pointer = std::make_shared<PostOffice>(post);
    return pointer;
}

PostOffice::PostOffice(unsigned GATENUMBER): gateNumber{GATENUMBER}{
    for(unsigned i = 0; i<gateNumber; i++){
        gatesStatus.push_back(nullptr);
    }
}

std::shared_ptr<IClient> PostOffice::getClient(const std::string& id){
        auto find = clients.find(id);
        if(find != clients.end())
        {
            return clients.at(id);
        }
        std::shared_ptr<IClient> client{new Client{id}};
        clients.insert(std::pair<std::string, std::shared_ptr<IClient>>(id, client));
        return client;
}

void PostOffice::enqueueClient(const std::shared_ptr<IClient> &client){
    if(!waitingQueue.empty()){
        std::vector<std::shared_ptr<IClient>> bufor;
        while(!waitingQueue.empty()){
            bufor.push_back(waitingQueue.top());
            waitingQueue.pop();
        }
        for(auto id: bufor){
            if(id->getIdNumber() == client->getIdNumber()){
                for(unsigned i = 0; i<bufor.size(); i++)
                    waitingQueue.push(bufor.at(i));
                return;
            }
        }
        for(unsigned i = 0; i<bufor.size(); i++)
            waitingQueue.push(bufor.at(i));
        }
    waitingQueue.push(client);
}

std::vector<std::string> PostOffice::getStatus(){
    std::vector<std::string> status;
    for(unsigned i = 0; i<gateNumber; i++){
       if(gatesStatus.at(i) != nullptr){
           status.push_back(gatesStatus.at(i)->getFullName());
       }
       else{
           status.push_back("");
       }
    }
    return status;
}

void PostOffice::gateReady(unsigned gate){
    if(gate>=gateNumber){
        throw IncorrectGateException {"Bad gate index."};
    }
    else{
        if(!waitingQueue.empty()){
            gatesStatus.at(gate) = waitingQueue.top();
            waitingQueue.pop();
        }
        else
            gatesStatus.at(gate) = nullptr;
    }
}

void PostOffice::collectPackages(unsigned gate){
    if(gate>=gateNumber || gatesStatus.at(gate) == nullptr){
        throw IncorrectGateException {"Bad gate index."};
    }
    else{
        gatesStatus.at(gate)->packagesCollected();
        gatesStatus.at(gate) = nullptr;
    }
}
