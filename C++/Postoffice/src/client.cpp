#include <iostream>
#include <fstream>
#include "client.h"
#include "exceptions.h"

Client::Client(const std::string id){
    idNumber = id;
    if(!readClientFromFile()){
       fullName = "";
       biometricData = "";
       priority = 0;
    }
}

std::string Client::getIdNumber(){return idNumber;}

std::string Client::getFullName(){return fullName;}

void Client::setFullName(const std::string& fullName){
    this->fullName = fullName;
}

int Client::getPriority(){return priority;}

void Client::updatePriority(const int priority){
    if(priority>0){
        this->priority = priority;
    }
    else
        this->priority = 0;
}

const std::string& Client::getBiometricData(){return biometricData;}

void Client::updateBiometricData(const std::string &biometricData){
    if(biometricDataHasLegalCharacters(biometricData)){
        this->biometricData = biometricData;
    }
    else{
        throw IncorrectBiometricDataException {biometricData + " has incorrect characters"};
    }
}

bool Client::verifyBiometricData(const std::string& biometricData, double treshold){
    if(biometricDataHasLegalCharacters(biometricData)){
        SmithWatermanAlg alg{biometricData,this->biometricData};
        if(alg()>treshold){
            return true;
        }
            return false;
    }
    else{
        throw IncorrectBiometricDataException {biometricData + "has incorrect characters"};
    }
    return false;
}

void Client::newPackage(const std::string& packageId){
    if(!isPackageExist(packageId)){
        packages.push_back(packageId);
    }else{
        throw PackageExistsException {packageId + " alredy exist!"};
    }
}

std::vector<std::string> Client::awaitingPackages(){
    return packages;
}

void Client::packagesCollected(){
    packages.clear();
}

bool Client::biometricDataHasLegalCharacters(const std::string biometricData){
    for(auto character:biometricData){
        if(character != 'A' && character != 'C' && character != 'G' && character != 'T'){
            return false;
        }
    }
    return true;
}

bool Client::isPackageExist(const std::string package){
    for(auto elem:packages){
        if(elem == package)
            return true;
    }
    return false;
}

Client::~Client(){
    std::ofstream file;
    file.open(idNumber, std::ofstream::out | std::ofstream::trunc);
    if(file.good()){
        file<<fullName<<std::endl;
        file<<biometricData<<std::endl;
        file<<priority<<std::endl;
        for(auto package: packages){
            file<<package<<std::endl;
        }
        file.close();
    }
}

bool Client::readClientFromFile(){
    std::ifstream file;
    file.open(idNumber, std::fstream::in);
    if(file.is_open()){
        std::string line;
        std::getline(file, line);
        fullName = line;
        std::getline(file, line);
        biometricData = line;
        std::getline(file, line);
        priority = (std::stoi(line));
        while(std::getline(file,line)){
            packages.push_back(line);
        }
        file.close();
        return true;
    }
    return false;
}
