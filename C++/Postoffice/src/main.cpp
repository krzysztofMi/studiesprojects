#include <iostream>
#include <assert.h>
#include "postoffice.h"
#include "client.h"
#include "smithwatermanalg.h"
using namespace std;

int main(){
    auto post_office = IPostOffice::create(5);
        auto client0 = post_office->getClient("9605279182");
        client0 ->setFullName("Jan Kowalski");
        auto client1 = post_office->getClient("69100839677");
        client1->setFullName("Adam Nowak");
        auto client2 = post_office->getClient("213213213");
        client2->setFullName("Adam Barnabeisz");
        client2->updateBiometricData("GGTTGACTA");
        client0->updatePriority(0);
        client1->updatePriority(1);
        client2->updatePriority(0);
        client0->newPackage("Paczk");
        client0->newPackage("Pacz");
        post_office->enqueueClient(client0);
        post_office->enqueueClient(client1);
        post_office->enqueueClient(client2);
        post_office->gateReady(3);
        auto status = post_office->getStatus();
        post_office->gateReady(3);
        status = post_office->getStatus();
        post_office->gateReady(3);
        return 0;

}
