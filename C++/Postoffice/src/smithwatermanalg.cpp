#include "smithwatermanalg.h"

SmithWatermanAlg::SmithWatermanAlg(const std::string seqA,const std::string seqB):
                                   sequenceA{seqA}, sequenceB{seqB}{
    matrixRow = seqA.length()+1;
    matrixColumn = seqB.length()+1;
    gapPanelty = 2;
    max = 0;
    for(unsigned i = 0; i<matrixRow; i++){
        matrix.push_back(std::vector<double>{});
        for(unsigned j = 0; j<matrixColumn; j++){
            matrix.at(i).push_back(0);
        }
    }
}

void SmithWatermanAlg::calcSubstitution(){
    for(unsigned i = 0; i<sequenceA.length(); i++){
        substitution.push_back(std::vector<double>{});
        for(unsigned j = 0; j<sequenceB.length(); j++){
            if(sequenceA.at(i)==sequenceB.at(j)){
                substitution.at(i).push_back(3);
            }
            else{
                substitution.at(i).push_back(-3);
            }
        }
    }
}

void SmithWatermanAlg::fillArray(unsigned i, unsigned j){
    array.at(0) = (matrix.at(i-1).at(j-1) + substitution.at(i-1).at(j-1));
    array.at(1) = (matrix.at(i-1).at(j) - gapPanelty);
    array.at(2) = (matrix.at(i).at(j-1) - gapPanelty);
    array.at(3) = (0);
}

double SmithWatermanAlg::findMaxInArray(){
    double max = array.at(0);
    index = 0;
    for(int i = 0; i<4; i++){
        if(max<array.at(i)){
            max = array.at(i);
            index = i;
        }
    }
    return max;
}

void SmithWatermanAlg::show(){
    int i = 0;
    for(auto row:matrix){
        std::cout<<i<<".";
        for(auto elem:row){
            std::cout<<"["<<elem<<"]";
        }
        i++;
        std::cout<<std::endl;
    }
}

void SmithWatermanAlg::findMaxInMatrix(unsigned i, unsigned j){
    if(matrix.at(i).at(j)>max){
        max = matrix.at(i).at(j);
    }
}

double SmithWatermanAlg::operator()(){
    calcSubstitution();
    for(unsigned i = 1; i<matrixRow; i++){
        for(unsigned j = 1; j<matrixColumn; j++){
            fillArray(i,j);
            matrix.at(i).at(j) = findMaxInArray();
            findMaxInMatrix(i, j);
        }
    }
    return max/getShorterSequence();
}

double SmithWatermanAlg::getShorterSequence(){
    return sequenceA.length() < sequenceB.length() ? sequenceA.length() : sequenceB.length();
}
