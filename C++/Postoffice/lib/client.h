#ifndef CLIENT_H
#define CLIENT_H
#include "iclient.h"
#include "smithwatermanalg.h"
#include <memory>
#include <map>
class Client : public IClient{
    std::string idNumber;
    std::string fullName;
    int priority;
    std::string biometricData;
    std::vector<std::string> packages;

    bool biometricDataHasLegalCharacters(const std::string);
    bool isPackageExist(std::string);
    void updateClientFile();
    bool readClientFromFile();
public:
    Client(const std::string);
    virtual ~Client();
    virtual std::string getIdNumber();
    virtual std::string getFullName();
    virtual void setFullName(const std::string&);
    virtual int getPriority();
    virtual void updatePriority(int);
    virtual const std::string& getBiometricData();
    virtual void updateBiometricData(const std::string&);
    virtual bool verifyBiometricData(const std::string&, double);
    virtual void newPackage(const std::string&);
    virtual std::vector<std::string> awaitingPackages();
    virtual void packagesCollected();
};
#endif // CLIENT_H
