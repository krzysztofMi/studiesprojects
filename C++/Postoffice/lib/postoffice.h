#ifndef POSTOFFICE_H
#define POSTOFFICE_H
#include "ipostoffice.h"
#include "client.h"
#include "less.h"
#include <queue>
#include <map>
class PostOffice: public IPostOffice{
    std::map<std::string, std::shared_ptr<IClient>> clients;
    unsigned gateNumber;    
    std::priority_queue<std::shared_ptr<IClient>,
                        std::vector<std::shared_ptr<IClient>>,
                        Less> waitingQueue;
    std::vector<std::shared_ptr<IClient>> gatesStatus;
public:
    PostOffice(unsigned);
    virtual ~PostOffice() = default;
    virtual std::shared_ptr<IClient> getClient(const std::string&);
    virtual void enqueueClient(const std::shared_ptr<IClient>&);
    virtual std::vector<std::string> getStatus();
    virtual void gateReady(unsigned);
    virtual void collectPackages(unsigned);
};

#endif // POSTOFFICE_H
