#ifndef SMITHWATERMANALG_H
#define SMITHWATERMANALG_H

#include <string>
#include <iostream>
#include <vector>
#include <array>
class SmithWatermanAlg
{
    std::vector<std::vector<double>> matrix;
    std::string sequenceA;
    std::string sequenceB;
    unsigned matrixColumn;
    unsigned matrixRow;
    std::vector<std::vector<double>> substitution;
    double gapPanelty;
    double max;
    unsigned index;
    std::array<double,4> array;

    void calcSubstitution();
    void fillArray(unsigned, unsigned);
    double findMaxInArray();
    void findMaxInMatrix(unsigned, unsigned);
    double getShorterSequence();
public:
    SmithWatermanAlg(const std::string,const std::string);
    void show();
    double operator()();
};

#endif // SMITHWATERMANALG_H
