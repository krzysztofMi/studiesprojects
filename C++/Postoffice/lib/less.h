#ifndef LESS_H
#define LESS_H
#include "iclient.h"
#include <memory>

struct Less{
public:
    bool operator()(const std::shared_ptr<IClient> &client1,
                   const std::shared_ptr<IClient> &client2) const;
};


#endif // LESS_H
