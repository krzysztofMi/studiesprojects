#ifndef WARHOUSE_H
#define WARHOUSE_H
#include <vector>
#include <stack>
#include <map>
#include <string>

class UniquePackage{
    static int key;
    std::map<std::string, int> uniquePackages;
public:
    void addUniquePackage(std::string);
    void removeUnquePackage(std::string);
    bool isUniquePackageInBase(std::string);
};

class Warhouse{
    const unsigned int m_maxShelfsCapacity;
    std::vector<std::stack<std::string>> m_shelfs;
    UniquePackage uniquePackage;
public:
    Warhouse(unsigned int, unsigned int);
    void acceptPackage(std::string);
    std::string issuePackage(std::string);
    void showWarhouse();
};

#endif // WARHOUSE_H
