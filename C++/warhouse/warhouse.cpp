#include <iostream>
#include "warhouse.h"

int UniquePackage::key = 0;
void UniquePackage::addUniquePackage(std::string package){
    if(uniquePackages.find(package)!=uniquePackages.end()){
        std::cout<<"This package is alredy in base.\n";
    }
    else{
        uniquePackages.insert(std::pair<std::string, int>(package, key));
        key ++;
    }
}

void UniquePackage::removeUnquePackage(std::string package){
    if(uniquePackages.find(package)!=uniquePackages.end()){
        uniquePackages.erase(package);
    }
    else{
        std::cout<<"This is no such element in base";
    }
}

bool UniquePackage::isUniquePackageInBase(std::string package){
    if(uniquePackages.find(package)!=uniquePackages.end()){
        return false;
    }
    return true;
}

Warhouse::Warhouse(unsigned int cabinet, unsigned int shelfes): m_maxShelfsCapacity{cabinet}{
    m_shelfs.reserve(shelfes);
    for(unsigned int i = 0; i<shelfes; i++)
        m_shelfs.push_back(std::stack<std::string>());
}

void Warhouse::acceptPackage(std::string package){
    if(uniquePackage.isUniquePackageInBase(package)){
        uniquePackage.addUniquePackage(package);
        unsigned int theLessGarbageShelf {0};
        unsigned int minimumPackageInShelfs {5};
        for(unsigned int i{0};i < m_shelfs.size(); i++){
            if( m_shelfs.at(i).size() < minimumPackageInShelfs){
                minimumPackageInShelfs = m_shelfs.at(i).size();
                theLessGarbageShelf = i;
            }
        }
        if(m_shelfs.at(theLessGarbageShelf).size() < m_maxShelfsCapacity)
            m_shelfs.at(theLessGarbageShelf).push(package);
        else
            std::cout<<"No more free cabinet in warhouse!\n";
    }
    else{
        std::cout<<"This package is alredy in the warhouse ;(\n";
    }
}

std::string Warhouse::issuePackage(std::string package){
    for(auto &elem : m_shelfs){
        std::vector<std::string> helpVector;
        while(elem.size()>0 && elem.top()!=package){
            helpVector.push_back(elem.top());
            elem.pop();
        }
        if(elem.size()>0 && elem.top()==package){
            elem.pop();
            for(auto & packageOnShelf: helpVector){
                elem.push(packageOnShelf);
            }
            uniquePackage.removeUnquePackage(package);
            return "Package found";
        }
        for(auto & packageOnShelf: helpVector){
            elem.push(packageOnShelf);
        }
    }
    return "There is no such a package in this warhouse";
}

void Warhouse::showWarhouse(){
    int i{0};
    for(auto &package : m_shelfs){
        std::vector<std::string> help;
        std::cout<<"Shelf: "<<i<<"\n";
        i++;
        while(package.size()>0){
           help.push_back(package.top());
           package.pop();
        }
        for(auto &back: help){
            std::cout<<back<<" ";
            package.push(back);
        }
        std::cout<<"\n";
    }
}
