#pragma once
#include <vector>
#include "efekty.h"
#include "table.h"

class Game{
    Table m_tab[2];
    Effects **m_effects;
    int m_x;
    int m_y;
    int m_dx;
    int m_dy;
    int m_e;
    int m_d;
    void applyEffect();
public:
    Game();
    void setTable();
    void show();
    ~Game();
};
