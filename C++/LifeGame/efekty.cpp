#include "efekty.h"

Effect0::Effect0(){};

void Effect0::apply(int x, int y, int dx, int dy, bool m_table[16][16]){
       for(int i = x; i<=dx; i++){
        for(int j = y; j<=dy; j++){
            m_table[i][j] = 0;
        }
    }
}


Effect1::Effect1(){}

void Effect1::apply(int x, int y, int dx, int dy, bool table[16][16]){
    for(int i = x; i<=dx; i++){
        for(int j = y; j<=dy; j++){
            table[i][j] = 1;
        }
    }
}



Effect2::Effect2(){}

void Effect2::apply(int x, int y, int dx, int dy, bool table[16][16]){
    for(int i = x; i<=dx; i++){
        for(int j = y; j<=dy; j++){
            if(table[i][j] == 1)
                table[i][j] = 0;
            else
                table[i][j] = 1;
        }
    }
}


Effect3::Effect3(){
     for(int i = 0; i<16; i++){
        for(int j =0; j<16; j++){
            h_table[i][j] = 0;
        }
    }
}

void Effect3::copyFrom(bool table[16][16]){
    for(int i = 0; i<16; i++){
        for(int j =0; j<16; j++){
            h_table[i][j] = table[i][j];
        }
    }
}

void Effect3::copyTo(bool table[16][16]){
      for(int i = 0; i<16; i++){
        for(int j =0; j<16; j++){
            table[i][j] = h_table[i][j];
            h_table[i][j] = 0;
        }
    }
}

void Effect3::apply(int x, int y, int dx, int dy, bool table[16][16]){
    this->copyFrom(table);
    for(int i = x; i<=dx; i++){
        for(int j = y; j<=dy; j++){
            if(table[i][j] == 1){
                int counter = -1;
                for(int p = i-1; p<=i+1; p++){
                    for(int d = j-1; d<=j+1; d++){
                        int c = p;
                        int v = d;
                        if(p == 16)
                            c = 0;
                        if(p == -1)
                            c = 15;
                        if(v == 16)
                            v = 0;
                        if(v == -1)
                            v = 15;
                        if(table[c][v] == 1){
                            counter ++;
                        }
                    }
                }
                if( !(counter ==2) && !(counter == 3)){
                    h_table[i][j] = 0;
                }
            }
        }
    }
    this->copyTo(table);
}



Effect4::Effect4():Effect3(){}





void Effect4::apply(int x, int y, int dx, int dy, bool table[16][16]){
    this->copyFrom(table);
    for(int i = x; i<=dx; i++){
        for(int j = y; j<=dy; j++){
            if(table[i][j] == 0){
                int counter = 0;

                for(int p = i-1; p<=i+1; p++){
                    for(int d = j-1; d<=j+1; d++){
                        int c = p;
                        int v = d;
                        if(p == 16)
                            c = 0;
                        if(p == -1)
                            c = 15;
                        if(v == 16)
                            v = 0;
                        if(v == -1)
                            v = 15;
                        if(table[c][v] == 1){
                            counter ++;
                        }
                    }
                }

                if(counter == 3){
                    h_table[i][j] = 1;
                }
                else{
                    h_table[i][j] = 0;
                }
            }
            else
                h_table[i][j] = 0;
        }
    }
    this->copyTo(table);

}

Effect5::Effect5():isTable(false){
     for(int i =0; i<16; i++){
        for(int j = 0; j<16; j++){
            h_table[i][j] = 0;
        }
    }
}

void Effect5::setZero(){
    for(int i =0; i<16; i++){
        for(int j = 0; j<16; j++){
            h_table[i][j] = 0;
        }
    }
}

void Effect5::apply(int x, int y, int dx, int dy, bool table[16][16]){
 if(isTable == false){
        for(int i = x; i<=dx; i++){
            for(int j = y; j<=dy; j++){
               if((table[i][j] == 0) && (h_table[i][j] == 0)){
                    h_table[i][j] = 0;
               }
               else if((table[i][j] == 1) && (h_table[i][j] == 0)){
                    h_table[i][j] = 1;
               }
               else if((table[i][j] == 0) && (h_table[i][j] == 1)){
                    h_table[i][j] = 1;
               }
               else if(table[i][j] == 1 && h_table[i][j] == 1){
                    h_table[i][j] = 0;
               }
            }
        }
        isTable = true;
    }
    else{
        for(int i = x; i<=dx; i++){
            for(int j = y; j<=dy; j++){
               if((table[i][j] == 0) && (h_table[i][j] == 0)){
                    table[i][j] = 0;
               }
               else if((table[i][j] == 1) && (h_table[i][j] == 0)){
                    table[i][j] = 1;
               }
               else if((table[i][j] == 0) && (h_table[i][j] == 1)){
                    table[i][j] = 1;
               }
               else if(table[i][j] == 1 && h_table[i][j] == 1){
                    table[i][j] = 0;
               }
            }
        }
        isTable = false;
        this->setZero();
    }
}


Effect6::Effect6():Effect5(){}



void Effect6::apply(int x, int y, int dx, int dy, bool table[16][16]){
  if(isTable == false){
        for(int i = x; i<=dx; i++){
            for(int j = y; j<=dy; j++){
                if(table[i][j] == 0){
                    h_table[i][j] = 0;
                }
                else{
                    h_table[i][j] = 1;
                }
            }
        }
        isTable = true;
    }
    else{
        for(int i = x; i<=dx; i++){
            for(int j = y; j<=dy; j++){
                if(h_table[i][j] == 0){
                    table[i][j] = 0;
                }
                else{
                    table[i][j] = 1;
                }
            }
        }
        isTable = false;
        this->setZero();
    }
}
