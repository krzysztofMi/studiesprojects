#include <iostream>
#include "table.h"



Table::Table(){
    for(int i = 0; i<16; i++){
        for(int j = 0; j<16; j++){
            m_table[i][j] = 0;
        }
    }
}

void Table::show(){
    for(int i = 0; i<16; i++){
        for(int j = 0;j<16; j++){
            std::cout<<m_table[i][j];
        }
        std::cout<<std::endl;
    }
}

void Table::apply(int x, int y, int dx, int dy, Effects &e){
    e.apply(x, y, dx, dy, m_table);
}
