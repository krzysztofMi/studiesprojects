#pragma once

class Effects{
public:
    virtual void apply(int, int, int, int, bool[16][16]) = 0;
};


class Effect0: public Effects{
public:
    Effect0();
    virtual void apply(int, int, int, int, bool [16][16]);
};


class Effect1:public Effects{
public:
    Effect1();
    virtual void apply(int, int, int, int, bool[16][16]);
};


class Effect2 : public Effects{
public:
    Effect2();
    virtual void apply(int, int, int, int, bool[16][16]);
};

class Effect3:public Effects{
protected:
    bool h_table[16][16];
    void copyTo(bool [16][16]);
    void copyFrom(bool [16][16]);
public:
    Effect3();
    virtual void apply(int, int, int, int, bool[16][16]);
};


class Effect4:public Effect3{
public:
    Effect4();
    virtual void apply(int, int, int, int, bool[16][16]);
};


class Effect5:public Effects{
protected:
    bool isTable;
    bool h_table[16][16];
public:
    Effect5();
    virtual void apply(int, int, int, int, bool [16][16]);
    void setZero();
};


class Effect6:public Effect5{

public:
    Effect6();
    virtual void apply(int, int, int, int, bool[16][16]);
};

