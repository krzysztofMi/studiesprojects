#include <iostream>
#include "game.h"
#include "efekty.h"



Game::Game():m_x{0}, m_y{0}, m_dx{0}, m_dy{0}, m_e{0}, m_d{0}{
    m_effects = new Effects*[7];
    m_effects[0] = new Effect0();
    m_effects[1] = new Effect1();
    m_effects[2] = new Effect2();
    m_effects[3] = new Effect3();
    m_effects[4] = new Effect4();
    m_effects[5] = new Effect5();
    m_effects[6] = new Effect6();
}


Game::~Game(){
    for(int i = 0; i<7; i++)
        delete []m_effects[i];
    delete m_effects;
}

void Game::show(){
    m_tab[m_d].show();
}

void Game::setTable(){
       while((std::cin>>m_x>>m_y>>m_dx>>m_dy>>m_e>>m_d) && !std::cin.eof()){
        while((m_x<0 || m_x>15 )||(m_y<0 || m_y>15 ) || (m_dx<m_x || m_dx>15 ) || (m_dy<m_y || m_dy>15 ) || (m_e<0 || m_e >15) || (m_d< 0 || m_d >1)){
                std::cin.clear();
                std::cin>>m_x>>m_y>>m_dx>>m_dy>>m_e>>m_d;
        }
        this->applyEffect();
    }
}

void Game::applyEffect(){
    m_tab[m_d].apply(m_x, m_y, m_dx, m_dy, *m_effects[m_e]);
}
