#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <limits>
#include <thread>
double fun(double x){
    return (1 - exp(-x))/x;
}

double taylorFun(double x, int n){
    double sum = 1;
    if(n == 1){
        return sum;
    }
    double next = (-x)/2.0;
    for(int i = 0;i<n; i++){
        sum+=next;
        next*=(-x)/(i+3.0);
    }
    return sum;
}

int main(){
    const double epsylon = 2.22044604925031308085e-16;

    std::ifstream filei("dane.txt");
    if(!filei){
        std::cerr<<"Bladd otwarcia pliku";
        exit(1);
    }
    std::vector<std::vector<double>> data;
    for(int i = 0; i<2; i++){
        data.push_back(std::vector<double>());
    }
    while(filei){
        for(int i = 0; i < 2; i++){
            double input;
            filei>>input;
            data.at(i).push_back(input);
        }
    }
    filei.close();
    std::ofstream fileo("error.txt");
    if(!fileo){
        std::cerr<<"Blad otwarcia pliku";
        exit(1);
    }

    for(int i = 0; i<(data.at(0).size()-1);i++){
        double score = fun(data.at(0).at(i));
	double score1 = taylorFun(data.at(0).at(i), 20);
        double error1 = fabs((score-data.at(1).at(i))/data.at(1).at(i));
	double error2 = fabs((score1-data.at(1).at(i))/data.at(1).at(i));
        /*if(error>epsylon){
            score = taylorFun(data.at(0).at(i), 20);
            error = fabs((score-data.at(1).at(i))/data.at(1).at(i));
        }*/
        fileo<<std::scientific<<"x:"<<data.at(0).at(i)<<" f(x):"<<data.at(1).at(i)<<" f(x)_W:"<<score<<" ErrorExp:"<<error1<<" ErrorTaylor:"<<error2<<std::endl;
    }
    fileo.close();

}
