#ifndef METODY_H
#define METODY_H


class Metody
{
    const double error = 1.0e-8;
    const double residuum = 1.0e-10;
    const int step = 100;
    typedef double(*funPtr)(double);
    funPtr fun = nullptr;
    void showX(double);
    bool sgn(double); // Return true if sign == -1 else return false;
    double derivative(double);

public:
    Metody(funPtr);
    void setFun(funPtr fun){ this->fun = fun;}
    void picard(double);
    void bisekcja(double, double);
    void newton(double);
    void secants(double, double);
};

#endif // METODY_H
