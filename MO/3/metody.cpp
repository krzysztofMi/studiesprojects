#include <iostream>
#include <cmath>
#include "metody.h"

Metody::Metody(funPtr fun){
    this->fun = fun;
}

void Metody::showX(double x){
    std::cout<<"Znaleziono przyblizone miejsce zerowe rowne: "<<x<<std::endl;
}

void Metody::picard(double firstX){
        double nextX = fun(firstX);
        double oldX = firstX;
    for(int i = 0; i<step; i++){
        oldX = nextX;
        nextX = fun(nextX);
        std::cout<<"Iteracja: "<<i<<"->"<<nextX<<std::endl;
        if(fabs(fun(nextX))<residuum){
           std::cout<<"Residuum\n";
           showX(nextX);
           return;
       }
        double errorP = fabs(oldX - nextX);
        if(errorP < error){
            std::cout<<"Błąd: error:"<<errorP <<"\n";
            showX(nextX);
            return;
        }
    }
    std::cout<<"Przekroczono liczbę iteracji.\n";
    showX(nextX);
}

bool Metody::sgn(double x){
    return (fun(x)<0) ? true:false;
}

void Metody::bisekcja(double a, double b){
    if(sgn(a)==sgn(b)){
        std::cout<<"Brak miejsc zerowyc w podanym przedziale.\n";
        return;
    }
    double nextX = a+(b-a)/2;
    for(int i = 0; i<step; i++){
        std::cout<<"Iteracja: "<<i<<"->"<<nextX<<std::endl;
        double c = a+(b-a)/2;
        if(sgn(a)!=sgn(c)){
            b = c;
        }else{
            a = c;
        }
        double errorB = (b-a)/2;
        nextX = a+(b-a)/2;
        if(error>fabs(errorB)){
            std::cout<<"Blad\n";
            showX(nextX);
            return ;
        }else if(fabs(fun(nextX))<residuum){
            std::cout<<"Residuum\n";
            showX(nextX);
            return ;
        }
    }
    std::cout<<"Koniec iteracji.\n";
    showX(nextX);
    return;
}

double Metody::derivative(double x){
    const double delta = 1e-10;
    return (fun(x+delta)-fun(x-delta))/(2*delta);
}

void Metody::newton(double x){
    double newX = fun(x);
    double oldX = newX;
    for(int i = 0; i<step; i++){
        oldX = newX;
        newX = oldX - (fun(oldX))/derivative(oldX);
        std::cout<<"Iteracja: "<<i<<"->"<<newX<<std::endl;
        double errorN = fabs(newX - oldX);
        if(errorN<error){
            std::cout<<"Error.\n";
            showX(newX);
            return;
        }else if(fabs(fun(newX))<residuum){
            std::cout<<"Residuum.\n";
            showX(newX);
            return;
        }
    }
    std::cout<<"Koniec iteracji.\n";
    showX(newX);
}

void Metody::secants(double x, double y){
    double nMinus1 = x;
    double n = y;
    double nPlus1 = n;
    for(int i = 0; i<step; i++){
        nPlus1 = n - fun(n)*(n - nMinus1)/(fun(n) - fun(nMinus1));
        nMinus1 = n;
        n = nPlus1;
        std::cout<<"Iteracja: "<<i<<"->"<<nPlus1<<std::endl;
        double errorS = fabs(nPlus1 - nMinus1);
        if(errorS<error){
            std::cout<<"Error.\n";
            showX(nPlus1);
            return;
        }else if(fabs(fun(nPlus1))<residuum){
            std::cout<<"Residuum.\n";
            showX(nPlus1);
            return;
        }
    }
    std::cout<<"Koniec iteracji.\n";
    showX(nPlus1);
}
