#include <iostream>
#include <cmath>
#include "metody.h"
using namespace std;

double fun1(double x){
    return sin(x/4)*sin(x/4);
}
double fun2(double x){
    return tan(2*x) - 1;
}

double test(double x){
    return exp(x)-1.5-atan(x);
}

int main(){
    Metody method(fun1);
    method.secants(1,5);
   // std::cout<<"Newton:\n";
   // method.newton(-7);
   // std::cout<<"Picard:\n";
   // method.picard(-10);
   // std::cout<<"Bisekcja:\n";
   // method.bisekcja(-10,10);
   // method.setFun(fun2);
    //std::cout<<"Picard:\n"<<method.picard(-10);
    return 0;
}
