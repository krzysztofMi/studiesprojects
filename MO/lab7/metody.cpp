#include "metody.h"
#include <iostream>
#include <array>
#include <algorithm>
#include <cmath>
Metody::Metody(std::array<std::array<double, 4>, 4> matrix, std::array<double, 4> vector){
    this->matrix = matrix;
    this->vector = vector;

    for(int i = 0; i<SIZE; i++){
        for(int j = 0; j<SIZE; j++){
            L.at(i).at(j) = D.at(i).at(j) = U.at(i).at(j) = 0;
            if(i==j){
                D.at(i).at(j) = matrix.at(i).at(j);
            }
            else if(i>j){
                L.at(i).at(j) = matrix.at(i).at(j);
            }
            else{
                U.at(i).at(j) = matrix.at(i).at(j);
            }
        }
    }
}


void Metody::jacobi(){
    std::cout<<"METODA JACOBIEGO.\n";
    //D^-1
    for(int i = 0; i<SIZE; i++){
        D.at(i).at(i) = 1/D.at(i).at(i);
    }
    //c
    std::array<double, 4> c;
    for(int i = 0; i<SIZE; i++){
        c.at(i) = D.at(i).at(i) * vector.at(i);
    }
    //M
    std::array<std::array<double, 4>, 4> m;
    for(int i = 0; i<SIZE; i++){
        for(int j = 0; j<SIZE; j++){
            m.at(i).at(j) = D.at(i).at(i) * (L.at(i).at(j) + U.at(i).at(j));
        }
    }
    std::array<double, 4> newX, oldX;
    newX = startX;
    for(int k = 0; k<STEP; k++){
        std::cout<<"Iteracja nr."<<k+1<<".\n";
        oldX = newX;
        for(int i = 0; i<SIZE; i++){
            double mx = 0;
            for(int j = 0; j<SIZE; j++){
                if(i !=j) mx += (m.at(i).at(j)*oldX.at(j));
            }
            newX.at(i) =-mx + c.at(i);
        }
        if(checkError(newX, oldX)){
            return;
        }
        else if(checkResiduum(newX)){
            return;
        }
    }
    std::cout<<"Koniec iteracji.\n";
    return;
}

void Metody::gauss_seidel(){
    std::cout<<"METODA GAUSSA-SEIDELA.\n";
    std::array<double, 4> newX, oldX;
    newX = startX;
    for(int k = 0; k<STEP; k++){
        oldX = newX;
        std::cout<<"Iteracja nr."<<k<<".\n";
        for(int i = 0; i<SIZE; i++){
            newX.at(i) = 0;
            for(int j = 0; j<SIZE; j++){
                if(i!=j){
                   if(j<i){
                       newX.at(i) += matrix.at(i).at(j)*newX.at(j);
                   }
                   if(j>i){
                       newX.at(i) += matrix.at(i).at(j)*oldX.at(j);
                   }
                }
            }
            newX.at(i) =(vector.at(i)-newX.at(i))/matrix.at(i).at(i);
        }
        if(checkError(newX, oldX)){
            return;
        }
        else if(checkResiduum(newX)){
            return;
        }
    }
    std::cout<<"Koniec iteracji.\n";
}

void Metody::sor(double omega){


    std::cout<<"METODA SOR.\n";
    std::array<double, 4> newX, oldX;
    newX = startX;
    for(int k = 0; k<STEP; k++){
        std::cout<<"Iteracja nr."<<k<<".\n";
        oldX = newX;
        for(int i = 0; i<SIZE; i++){
            double sum0, sum1;
            sum0 = sum1 =0.0;
            for(int j = 0; j<SIZE; j++){
                if(i!=j){
                    if(j<i){
                        sum0 += matrix.at(i).at(j)*newX.at(j);
                    }else{
                        sum1 += matrix.at(i).at(j)*oldX.at(j);
                    }
                }
            }
            newX[i] =(1.0 - omega)*oldX.at(i) + omega*(vector.at(i) - sum0 - sum1)/matrix.at(i).at(i);
        }
        if(checkError(newX, oldX)){
            return;
        }
        else if(checkResiduum(newX)){
            return;
        }
    }
    std::cout<<"Koniec iteracji.\n";
}

bool Metody::checkError(std::array<double, 4> newX,
                        std::array<double, 4> oldX){
    std::array<double, 4> errors;
    for(int i = 0; i<SIZE; i++){
        errors[i] = fabs(newX[i] - oldX[i]);
    }
    std::cout<<"Bledy rozwiazan: ";
    showVector(errors);
    double maxError = calculateMax(errors);
    if(maxError<ERROR){
        std::cout<<"\nKoniec iteracji: osiagnieto oczekiwany minimalny blad.\n";
        std::cout<<"\tZnalezione rozwiazanie = ";
        showVector(newX);
        return true;
    }
    return false;
}

bool Metody::checkResiduum(std::array<double, 4> newX){
    std::array<double, 4> fx;
    fx[0] = fabs(100*newX[0] + newX[1] - 2*newX[2] + 3*newX[3] - 395);
    fx[1] = fabs(4*newX[0] + 300*newX[1] - 5*newX[2] + 6*newX[3] - 603);
    fx[2] = fabs(7*newX[0] - 8*newX[1] - 400*newX[2] + 9*newX[3] + 415);
    fx[3] = fabs(-(10*newX[0]) + 11*newX[1] - 12*newX[2] + 200*newX[3] + 606);
    std::cout<<"Residuum :";
    showVector(fx);
    double maxResiduum = calculateMax(fx);
    if(maxResiduum<RESIDUUM){
        std::cout<<"\nKoniec iteracji: osiagnieto oczekiwane residuum.\n";
        std::cout<<"\tZnalezione rozwiazanie = ";
        showVector(newX);
        return true;
    }
    return false;
}

double Metody::calculateMax(std::array<double, 4> vec){
    return *std::max_element(vec.begin(), vec.end());
}

void Metody::showMatrix(std::array<std::array<double, 4>, 4> matrix){
    for(std::array<double, 4> array: matrix){
        for(double x: array){
            std::cout<<x<<" ";
        }
        std::cout<<"\n";
    }
    std::cout<<std::endl;
}

void Metody::showVector(std::array<double, 4> vec){
    for(double x: vec)
        std::cout<<x<<" ";
    std::cout<<std::endl;
}

double Metody::det(std::array<std::array<double, 3>,3> matrix){
    double d = matrix.at(0).at(0)*matrix.at(1).at(1)*matrix.at(2).at(2) +
               matrix.at(1).at(0)*matrix.at(2).at(1)*matrix.at(0).at(2) +
               matrix.at(2).at(0)*matrix.at(0).at(1)*matrix.at(1).at(2) -
               matrix.at(0).at(2)*matrix.at(1).at(1)*matrix.at(2).at(0) -
               matrix.at(1).at(2)*matrix.at(2).at(1)*matrix.at(0).at(0) -
               matrix.at(2).at(2)*matrix.at(0).at(1)*matrix.at(1).at(0);
    return d;
}
