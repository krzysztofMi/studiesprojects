#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>

class DifferentialEquation{
    double U(double t){
        return (1 - exp(-10*(t+atan(t))));
    }
    double h;
    double t;
    std::ofstream file;
public:
    DifferentialEquation(double h, double t){
        this->h = h;
        this->t = t;
        file.open("AnaliticData.dat");
        if(!file.is_open()){
            std::cerr<<"Blad otwarcia pliku.\n";
            exit(1);
        }
        for(double i = 0.0; i<t; i+=h){
            file<<i<<" "<<U(i)<<std::endl;
        }
        file.close();
    }

    double directEulerMethod(){
        double y = 0.0;
        file.open("DEM.dat");
        if(!file.is_open()){
            std::cerr<<"Blad otwarcia pliku.\n";
            exit(1);
        }
        double maxError = 0.0;
        for(double i = 0.0; i<t; i+=h){
            y = y - (10.0*i*i+20.0)/(i*i+1.0)*(y-1.0)*h;
            file<<i<<" "<<y<<"\n";
            double error = fabs(y-U(i));
            if(error>maxError){
                maxError = error;
            }
        }
        file.close();
        return maxError;
    }

    double indirectEulerMethod(){
        double y = 0.0;
        file.open("IEM.dat");
        if(!file.is_open()){
            std::cerr<<"Blad otwarcia pliku.\n";
            exit(1);
        }
        double maxError = 0.0;
        for(double i = 0.0; i<t; i+=h){
            double t1 = i+h;
            double ft1 = (10.0*t1*t1+20.0)/(t1*t1+1);
            y = (y/h + ft1)/(1.0/h+ft1);
            file<<i<<" "<<y<<std::endl;
            double error = fabs(y-U(i));
            if(error>maxError){
                maxError = error;
            }
        }
        file.close();
        return maxError;
    }

    double indirectTrapeziumMethod(){
        double y = 0.0;
        file.open("ITM.dat");
        if(!file.is_open()){
            std::cerr<<"Blad otwarcia pliku.\n";
            exit(1);
        }
        double maxError = 0.0;
        for(double i = 0.0; i<t; i+=h){
            double t1 = i+h;
            double ft = (10.0*i*i+20.0)/(2*i*i+2);
            double ft1 = (10.0*t1*t1+20.0)/(2*t1*t1+2);
            double error = fabs(y-U(i));
            if(error>maxError){
                maxError = error;
            }
            y = (y/h - ft*(y-1)+ft1)/(1.0/h + ft1);
            file<<i<<" "<<y<<std::endl;

        }
        file.close();
        return maxError;
    }

    void setH(double h){
        this->h = h;
    }
};

int main(){
    double t = 3;
    double h = 0.001;
    DifferentialEquation equation(h, t);

    std::ofstream file;
    file.open("Error.dat");
    if(!file.is_open()){
        std::cerr<<"Blad otwarcia pliku.\n";
        exit(1);
    }
    int j = 0;
    for(double i = 0.000001; i<t; i+=h){
        equation.setH(i);
        file<<i<<" "<<equation.directEulerMethod()<<" "
           <<equation.indirectEulerMethod()<<" "<<equation.indirectTrapeziumMethod()<<std::endl;
    }
    file.close();
    return 0;
}
