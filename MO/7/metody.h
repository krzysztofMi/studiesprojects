#pragma once
#include <array>


class Metody{
    std::array<std::array<double,4>, 4> matrix, L, D, U;
    std::array<double, 4> vector, startX = {1,1,1,1};
    const int SIZE = 4;
    const double ERROR = 10e-7;
    const double RESIDUUM = 10e-7;
    const int STEP = 100;
public:
    Metody(std::array<std::array<double, 4>, 4> matrix,
           std::array<double, 4> vector);

    void jacobi();
    void gauss_seidel();
    void sor(double);

private:
    void showMatrix(std::array<std::array<double, 4>, 4>);
    void showVector(std::array<double, 4>);
    double calculateMax(std::array<double, 4>);
    bool checkError(std::array<double, 4>,
                    std::array<double, 4>);
    bool checkResiduum(std::array<double, 4>);
    double det(std::array<std::array<double, 3>,3> matrix);
};
