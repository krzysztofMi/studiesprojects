#include <iostream>
#include <array>
#include "metody.h"

using namespace std;

int main(){
    array<array<double, 4>, 4> matrix;
    matrix.at(0).at(0) = 100.0; matrix.at(0).at(1) = 1.0; matrix.at(0).at(2) = -2.0; matrix.at(0).at(3) = 3.0;
    matrix.at(1).at(0) = 4.0; matrix.at(1).at(1) = 300.0; matrix.at(1).at(2) = -5.0; matrix.at(1).at(3) = 6.0;
    matrix.at(2).at(0) = 7.0; matrix.at(2).at(1) = -8.0; matrix.at(2).at(2) = 400.0; matrix.at(2).at(3) = 9.0;
    matrix.at(3).at(0) = -10.0; matrix.at(3).at(1) = 11.0; matrix.at(3).at(2) = -12.0; matrix.at(3).at(3) = 200.0;

    array<double, 4> vector;
    vector.at(0) = 395.0;
    vector.at(1) = 603.0;
    vector.at(2) = -415.0;
    vector.at(3) = -606.0;

    array<double, 4> x0;
    x0.at(0) = 1;
    x0.at(1) = 1;
    x0.at(2) = 1;
    x0.at(3) = 1;
    Metody metods(matrix, vector);
    metods.jacobi();
    metods.gauss_seidel();
    metods.sor(0.5);
    return 0;
}




