#include <iostream>

template<typename T>
void epsylon(T liczba){
    int bityMantysy = 0;
    while(liczba+1!=1){
         bityMantysy ++;
         liczba/=2;
    }
    std::cout<<"Liczba bitow mantysy: "<<--bityMantysy<<std::endl;
    std::cout<<"Epsylon: "<<liczba*2<<std::endl;
}


int main(){
    std::cout<<"Float: ";
    epsylon(1.0f);
    std::cout<<"Double: ";
    epsylon(1.0);
    return 0;
}
