template <class T>
class Difference{
    T a, b, x;
    T fun(T x){
        return cos(x/2);
    }
    public:
    Difference(T a, T b, T x){
        try{
            if(x<a || x>b){
                std::string boo {"Podane x nie nalezy do siatki.\n"};
                throw boo;
            }
            this->a = a;
            this->b = b;
            this->x = x;
        }catch(std::string &ex){
            std::cout<<ex;
            exit(0);
        }
    }

    //For derivatives where x is in inside node.
    T forawardDifference(T h){
        if(h!=0 && x>a && x<b){
            return (fun(x+h) - fun(x))/h;
        }
        return -100;
    }
    T backwardDifference(T h){
        if(h!=0 && x>a && x<b){
            return (fun(x)-fun(x-h))/h;
        }
        return -100;
    }
    T centralDifference(T h){
        if(h!=0 && x>a && x<b){
            return (fun(x+h) - fun(x-h))/(2*h);
        }
        return -100;
    }
    //To calculate derivative where x is in first or end node
    T firstOrEndNodeDerivativeTwoPoint(T h){
        if(h!=0 && x==a){
            return (fun(a+h)-fun(a))/h;
        }
        else if(h!=0 && x==b){
            return (fun(b)-fun(b-h))/h;
        }
        return -100;
    }
    T firstOrEndNodeDerivativeThreePoint(T h){
        if(h!=0 && x==a){
            return (-3.0/2.0*fun(a)+2*fun(a+h)-0.5*fun(a+2*h))/h;
        }
        else if(h!=0 && x==b){
            return (0.5*fun(b-2*h)-2*fun(b-h)+3.0/2.0*fun(b))/h;
        }
        return -100;
    }

    void changeX(T x){
        try{
            if(x<a || x>b){
                std::string x{"Podane x wychodzi poza siatke.\nX nie zostalo zmienione.\n"};
                throw x;
            }
            this->x = x;
        }catch(std::string &ex){
            std::cout<<ex;
        }
    }
};
