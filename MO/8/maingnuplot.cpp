#include<iostream>
#include "gnuplot.h"



int main(){
    Gnuplot plt;
    plt("set term postscript eps color");
    plt("set output \"a.eps\" ");
    plt("set logscale xy");
    plt("plot \'./dataDouble.dat\' u 1:2 w l lt rgb 'greenyellow' t 'forward', \
              \'./dataDouble.dat\' u 1:3 w l lt rgb 'light-pink' t 'backward', \
              \'./dataDouble.dat\' u 1:4 w l lt rgb 'black' t 'central', \
              \'./dataDouble.dat\' u 1:5 w l lt rgb 'royalblue' t 'firstNTwo', \
              \'./dataDouble.dat\' u 1:6 w l lt rgb 'orangered4' t 'firstNThree', \
              \'./dataDouble.dat\' u 1:7 w l lt rgb 'dark-magenta' t 'lastNTwo', \
              \'./dataDouble.dat\' u 1:8 w l lt rgb 'salmon' t 'lastNThree'");
    plt("plot \'./dataFloat.dat\' u 1:2 w l lt rgb 'greenyellow' t 'forward', \
              \'./dataFloat.dat\' u 1:3 w l lt rgb 'light-pink' t 'backward', \
              \'./dataFloat.dat\' u 1:4 w l lt rgb 'black' t 'central', \
              \'./dataFloat.dat\' u 1:5 w l lt rgb 'royalblue' t 'firstNTwo', \
              \'./dataFloat.dat\' u 1:6 w l lt rgb 'orangered4' t 'firstNThree', \
              \'./dataFloat.dat\' u 1:7 w l lt rgb 'dark-magenta' t 'lastNTwo', \
              \'./dataFloat.dat\' u 1:8 w l lt rgb 'salmon' t 'lastNThree'");
}
