#include <iostream>
#include <vector>
#include <array>
#include <cmath>

double f1(double x, double y, double z){
    return x*x+y*y+z*z - 2;
}
double f1px(double x){
    return 2*x;
}
double f1py(double y){
    return 2*y;
}
double f1pz(double z){
    return 2*z;
}
double f2(double x, double y){
    return x*x+y*y-1;
}
double f2px(double x){
    return 2*x;
}
double f2py(double y){
    return 2*y;
}
double f2pz(){
    return 0;
}
double f3(double x, double y){
    return x*x-y;
}
double f3px(double x){
    return 2*x;
}
double f3py(){
    return -1;
}
double f3pz(){
    return 0;
}

void showMatrix(std::array<std::array<double,3>,3>);
void showVector(std::array<double,3>);
double det(std::array<std::array<double,3>, 3>);

int main(){
    const double RESIDUUM = 1e-6;
    const double ERROR = 1e-6;
    const double STEP = 100;
    std::array<double, 3> fx;
    std::array<std::array<double, 3>, 3> jacobian;
    std::array<double, 3> newX, oldX;
    newX[0] = oldX[0] = 1;
    newX[1] = oldX[1] = 2;
    newX[2] = oldX[2] = 3;
    for(int i = 0; i<STEP; i++){
        for(int i = 0; i<3; i++){
            oldX[i] = newX[i];
        }
        fx[0] = f1(oldX[0], oldX[1], oldX[2]);
        fx[1] = f2(oldX[0], oldX[1]);
        fx[2] = f3(oldX[0], oldX[1]);
        jacobian[0][0] = f1px(oldX[0]); jacobian[0][1] = f1py(oldX[1]); jacobian[0][2]=f1pz(oldX[2]);
        jacobian[1][0] = f2px(oldX[0]); jacobian[1][1] = f2py(oldX[1]); jacobian[1][2]=f2pz();
        jacobian[2][0] = f3px(oldX[0]); jacobian[2][1] = f3py(); jacobian[2][2]=f3pz();

        std::array<std::array<double, 3>, 3> Ax0 = jacobian;
        std::array<std::array<double, 3>, 3> Ax1 = jacobian;
        std::array<std::array<double, 3>, 3> Ax2 = jacobian;
        for(int i = 0; i<3; i++){
            Ax0[i][0] = fx[i];
            Ax1[i][1] = fx[i];
            Ax2[i][2] = fx[i];
        }
        double detA = det(jacobian);
        double detAx0 = det(Ax0);
        double detAx1 = det(Ax1);
        double detAx2 = det(Ax2);
        newX[0] = oldX[0] - detAx0/detA;
        newX[1] = oldX[1] - detAx1/detA;
        newX[2] = oldX[2] - detAx2/detA;
        std::cout<<"Iteracja numer " << i <<".->";
        showVector(newX);
        double error0 = fabs(newX[0] - oldX[0]);
        double error1 = fabs(newX[1] - oldX[1]);
        double error2 = fabs(newX[2] - oldX[2]);
        if(error0<ERROR && error1<ERROR && error2<ERROR){
            std::cout<<"Blad.\n";
            std::cout<<"Przyblizony wynik rownania wynosi: "; showVector(newX);
            return 0;
        }
        if(fabs(f1(newX[0], newX[1], newX[2]))<RESIDUUM &&
                fabs(f2(newX[0], newX[1]))<RESIDUUM &&
                fabs(f3(newX[0], newX[1]))<RESIDUUM){
            std::cout<<"Residuum.\n";
            std::cout<<"Przyblizony wynik rownania wynosi: "; showVector(newX);
            return 0;
         }
    }
    std::cout<<"Koniec iteracji.\n";
    std::cout<<"Przyblizony wynik rownania wynosi: "; showVector(newX);
    return 0;
}


void showMatrix(std::array<std::array<double,3>,3> matrix){
    for(std::array<double, 3> vec: matrix){
        for(double x: vec){
            std::cout<<x<<" ";
        }
        std::cout<<"\n";
    }
}

void showVector(std::array<double,3> vec){
    std::cout<<"[";
    for(double x: vec){
        std::cout<<x<<", ";
    }
    std::cout<<"\b\b]\n";
}

double det(std::array<std::array<double, 3>,3> matrix){
    double d = matrix.at(0).at(0)*matrix.at(1).at(1)*matrix.at(2).at(2) +
               matrix.at(1).at(0)*matrix.at(2).at(1)*matrix.at(0).at(2) +
               matrix.at(2).at(0)*matrix.at(0).at(1)*matrix.at(1).at(2) -
               matrix.at(0).at(2)*matrix.at(1).at(1)*matrix.at(2).at(0) -
               matrix.at(1).at(2)*matrix.at(2).at(1)*matrix.at(0).at(0) -
               matrix.at(2).at(2)*matrix.at(0).at(1)*matrix.at(1).at(0);
    return d;
}
