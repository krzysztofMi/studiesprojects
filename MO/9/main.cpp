#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include <algorithm>
//d^2U(x)/(dx^2) + 4*U(x) + tan(x) = 0

double U(double x){
    return (2*x*cos(2*x)+2*sin(2*x)-log(2)*sin(2*x)-2*log(cos(x))*sin(2*x))/4.0;
}

class Discretization{
    std::vector<double> l, d, u, b, xn;
    double x1, x2;
    double h;
    int numberOfSteps;
    char c;
    double U(double x){
        return 1.0/4.0*(2*x*cos(2*x)+2*sin(2*x)-log(2)*sin(2*x)-2*log(cos(x))*sin(2*x));
    }

    double maxError(){
        double max = 0.0;
        for(int i = 0; i<numberOfSteps; i++){
            double error = fabs(xn.at(i) - U(h*i));
            if(max<error){
                max = error;
            }
        }
        return max;
    }

    void clearVectors(){
        l.clear();
        d.clear();
        u.clear();
        b.clear();
        xn.clear();
    }
public:
    Discretization(int numberOfSteps){
        this->numberOfSteps = numberOfSteps;
        x1 = 0.0;
        x2 = M_PI/4.0;
        h = fabs(x1-x2)/(numberOfSteps-1);
    }
    void thomasAlgorithm(){
        std::vector<double> n, r;
        n.push_back(d.at(0));
        r.push_back(b.at(0));
        for(int i = 1; i<numberOfSteps; i++){
            n.push_back(d.at(i)-(l.at(i)*u.at(i-1))/n.at(i-1));
            r.push_back(b.at(i)-(l.at(i)*r.at(i-1))/n.at(i-1));
        }
        xn.push_back(1.0/n.at(numberOfSteps-1)*r.at(numberOfSteps-1));
        int j = 0;
        for(int i = numberOfSteps-2; i>=0; i--){
            xn.push_back((r.at(i)-u.at(i)*xn.at(j++))/n.at(i));
        }
        std::reverse(xn.begin(), xn.end());
    }
    double conventionalDiscretization(){
        clearVectors();
        l.push_back(0.0);
        d.push_back(1.0);
        u.push_back(0.0);
        b.push_back(0.0);
        for(int i = 1; i<numberOfSteps-1; i++){
            l.push_back(1.0/(h*h));
            d.push_back(4.0-2.0/(h*h));
            u.push_back(1.0/(h*h));
            b.push_back(-tan(h*i));
        }
        l.push_back(0.0);
        d.push_back(1.0);
        u.push_back(0.0);
        b.push_back(0.5);
        thomasAlgorithm();
        return maxError();
    }

    double numerovDiscretization(){
        clearVectors();
        l.push_back(0.0);
        d.push_back(1.0);
        u.push_back(0.0);
        b.push_back(0.0);
        for(int i = 1; i<numberOfSteps-1; i++){
            l.push_back(4.0/12.0 + 1.0/(h*h));
            d.push_back(40.0/12.0 - 2.0/(h*h));
            u.push_back(4.0/12.0 + 1.0/(h*h));
            b.push_back(-(tan(h*(i-1))/12.0 + (10.0*tan(h*i))/12.0 + tan(h*(i+1))/12.0));
        }
        l.push_back(0.0);
        d.push_back(1.0);
        u.push_back(0.0);
        b.push_back(0.5);
        thomasAlgorithm();
        return maxError();
    }

    double getH(){
        return h;
    }
    void setNumberOfSteps(int numberOfSteps){
        this->numberOfSteps = numberOfSteps;
        h = fabs(x1-x2)/(numberOfSteps-1);
    }
    double getNumberOfSteps(){
        return numberOfSteps;
    }
};

int main(){
    int steps = 10;
    Discretization normal(steps);
    std::ofstream file;
    file.open("Data1.dat");
    if(!file.is_open()){
        std::cerr<<"File wasn't open!";
        exit(0);
    }
    while(normal.getNumberOfSteps()<10000){
        file<<log10(normal.getH())<<" "<<log10(normal.conventionalDiscretization())<<" "<<log10(normal.numerovDiscretization())<<"\n";
        steps+=100;
        normal.setNumberOfSteps(steps);
    }
    normal.numerovDiscretization();
    file.close();
    return 0;
}
