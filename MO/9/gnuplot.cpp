#include <iostream>
#include <string>
#include "gnuplot.h"

Gnuplot::Gnuplot(){
    gnuplotpipe = popen("gnuplot -persist", "w");
    if(!gnuplotpipe)
        std::cerr<<("Gluplot not found!");
}
Gnuplot::~Gnuplot(){
    fprintf(gnuplotpipe, "exit\n");
    pclose(gnuplotpipe);
}
void Gnuplot::operator() (const std::string &command){
    fprintf(gnuplotpipe, "%s\n", command.c_str());
    fflush(gnuplotpipe);
}
