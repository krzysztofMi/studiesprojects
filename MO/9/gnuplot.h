#pragma once
#include <string>
#include <iostream>
class Gnuplot{
public:
    Gnuplot();
    ~Gnuplot();
    void operator () (const std::string & command);
protected:
    FILE *gnuplotpipe;
};
