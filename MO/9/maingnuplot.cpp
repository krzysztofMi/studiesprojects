#include<iostream>
#include "gnuplot.h"



int main(){
    Gnuplot plt;
    plt("set term postscript eps color");
    plt("set output \"a.eps\" ");
    plt("set logscale xy");
    plt("plot \'./Data.dat\' u 1:2 w l lt rgb 'greenyellow' t 'Conventional' ,\
              \'./Data.dat\' u 1:3 w l lt rgb 'light-pink' t 'Numerov'");
}
